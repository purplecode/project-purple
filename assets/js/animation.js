var TimelineMax = new TimelineMax({paused: true});
TimelineMax.to(" .title", 1, {
      opacity: 0
});
TimelineMax.to(".menu", 2, {
      width: "50%",
      ease: Expo.easeOut
}, "-=1");
TimelineMax.staggerFrom(".menu ul li", 2, {y: 40  , opacity: 0, ease: Expo.easeOut}, 0.3);
TimelineMax.reverse();
$(document).on("click", ".toggle-btn", function() {
      TimelineMax.reversed(!TimelineMax.reversed());
});
